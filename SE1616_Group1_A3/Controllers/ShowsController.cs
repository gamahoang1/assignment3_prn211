﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SE1616_Group1_A3.Code;
using SE1616_Group1_A3.Models;

namespace SE1616_Group1_A3.Controllers
{
    public class ShowsController : Controller
    {
        private readonly CinemaContext _context;

        public ShowsController(CinemaContext context)
        {
            _context = context;
        }

        // GET: Shows

        private int?[] getSlotsByDateEdit(DateTime date, int roomId, int currentSlot)
        {
            int?[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int?[] slots = _context.Shows.Where(s => s.RoomId == roomId && s.ShowDate == date).Select(s => s.Slot).ToArray();
            foreach (int number in slots)
            {
                int numToRemove = number;
                if (numToRemove != currentSlot)
                    a = a.Where(val => val != numToRemove).ToArray();
            }
            return a;
        }
        public async Task<IActionResult> Index(Login user, Show show)
        {
            ViewBag.Film = _context.Films.ToList();
            ViewBag.Room = _context.Rooms.ToList();
           
            if (show.FilmId == 0)
            {
                ViewBag.Show = _context.Shows.ToList();
            }
            else
            {
                ViewBag.Show = _context.Shows.Where(x => x.FilmId == show.FilmId
                && x.RoomId == show.RoomId && x.ShowDate == show.ShowDate).ToList<Show>();
            }
            
            if (ModelState.IsValid)
            {
                string name, pass;
                var conf = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", true, true)
                    .Build();
                name = conf["User:Name"];
                pass = conf["User:Password"];
                //HttpContext.Session.SetInt32("age", 20);
                //HttpContext.Session.SetString("username", "abc");                
                
                if (user.UserName.Equals(name) && user.Password.Equals(pass))
                {
                    Login lw = new Login(name, pass);
                    SessionHelper.SetObjectAsJson(HttpContext.Session, "User", lw);

                }
                else
                {
                    TempData["message"] = "User or Password is not correct";
                    return Redirect("/Home/Index");
                }
                

                
            }
            return View();


        }
       

        // GET: Shows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var show = await _context.Shows
                .FirstOrDefaultAsync(m => m.ShowId == id);
            if (show == null)
            {
                return NotFound();
            }

            return View(show);
        }

        // GET: Shows/Create
        public IActionResult Create(string dob,int room=1,int film=1)
        {
            if(dob == null)
            {
                dob = DateTime.Now.ToString("yyyy-MM-dd"); 
            }
            DateTime date = Convert.ToDateTime(dob);
            ViewBag.Film = _context.Films.ToList();
            ViewBag.Room = _context.Rooms.ToList();
            ViewBag.Show = _context.Shows.ToList();
            ViewBag.Slot = _context.Shows.Where(s=> s.ShowDate== date && s.RoomId==room && s.FilmId==film ).ToList();
            return View();
        }

        // POST: Shows/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ShowId,RoomId,FilmId,ShowDate,Price,Status,Slot")] Show show)
        {
            if (ModelState.IsValid)
            {
                _context.Add(show);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(show);
        }

        // GET: Shows/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var show = await _context.Shows.FindAsync(id);
            if (show == null)
            {
                return NotFound();
            }
            int?[] slots = getSlotsByDateEdit((DateTime)show.ShowDate, show.RoomId, (int)show.Slot);
            ViewData["FilmId"] = new SelectList(_context.Films, "FilmId", "Title", show.FilmId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "RoomId", "Name", show.RoomId);
            ViewData["slot"] = new SelectList(slots);
            return View(show);
        }

        // POST: Shows/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ShowId,RoomId,FilmId,ShowDate,Price,Status,Slot")] Show show)
        {
            if (id != show.ShowId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(show);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShowExists(show.ShowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FilmId"] = new SelectList(_context.Films, "FilmId", "CountryCode", show.FilmId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "RoomId", "RoomId", show.RoomId);
            return View(show);
        }

        // GET: Shows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var show = await _context.Shows
                .FirstOrDefaultAsync(m => m.ShowId == id);
            if (show == null)
            {
                return NotFound();
            }

            return View(show);
        }

        // POST: Shows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var show = await _context.Shows.FindAsync(id);
            _context.Shows.Remove(show);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ShowExists(int id)
        {
            return _context.Shows.Any(e => e.ShowId == id);
        }
    }
}
