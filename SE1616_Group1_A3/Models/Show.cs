﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


#nullable disable

namespace SE1616_Group1_A3.Models
{
    public partial class Show
    {
        public int ShowId { get; set; }
        public int RoomId { get; set; }
        public int FilmId { get; set; }
        public DateTime? ShowDate { get; set; }
        [Range(1, 999999)]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]


        public decimal? Price { get; set; }

        public bool? Status { get; set; }
        public int? Slot { get; set; }
    }
}
