﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SE1616_Group1_A3.Models
{
    public partial class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
    }
}
