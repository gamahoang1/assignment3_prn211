﻿using System.ComponentModel.DataAnnotations;

namespace SE1616_Group1_A3.Models
{
    public class Login
    {
        [Required(ErrorMessage = "User Name is Required")]    
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is Required")]
        public string Password { get; set; }

        public Login(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        public Login()
        {
        }
    }
}
